��    O      �  k         �     �     �     �     �     �  )        +     1     8  0   G  #   x     �     �     �     �  
   �     �     �               +     B  2   N  H   �      �     �     �  "   	     )	     ;	     C	     K	     k	     p	  
   �	  6   �	     �	  
   �	     �	     
     
     
     '
  I   -
     w
     |
     �
     �
     �
  
   �
     �
     �
     �
  5   �
                         "     +     2     9     J     V     n     s     �     �     �     �      �     �     	          *     /  +   H     t  �   x     N     n     s     �     �  /   �  	   �     �     �  >   �  *   >     i     y     �     �  	   �     �     �     �     �            D   ,  \   q      �  	   �     �  (        5     L     T  -   [     �     �     �  =   �       
              3     8     M     c  G   i     �     �     �     �     �     �     �     �       ?        Z     a     m     t     y     �  	   �     �     �  $   �     �  "   �  "        *     2     G  $   d     �     �     �     �  !   �  7   �          2                (   0                   =          9   .   6       '   /      $      3   "   8      <   M                             7       5                  K   !   ,   N         #          O                     C      4   L   %   F   B       I      
   -      E           +                 H   G      ?   J   &   A          1       >   	   @      D      )      *             ;          :    
Can't Create Directory  
In:   Exist on Server 21 : Rename Remote File ?. Onle empty directories can be deleted. About Active After Connect: Can't open downloaded file or file doesn't exist Can't remember a unnamed connection Carpeta Remota: Close Connection Close Current Connection Connect Connection Connections Manager Create New Directory Delete Delete Directory  Delete Empty Directory Delete File Delete selected remote files? This can't be undone Deletion failed. Check if directory is empty and permissions are enough. Descargar Archivos Seleccionados Download Download Canceled Download Directory and All Content Download Selected Error:
 Error:  Error: Can't rename Remote File Exit FTP Client, Made with Gambas 3 File Name  Ftp Client made In Gambas. Like Filezilla, but simpler GBFTP Gambas FTP Gambas ftp: Connections Manager Host Known Connections Last Modified Local Made in 2021-2022 by Jorge Carrión (shordi@gmail.com) without Copyrights Modo Name Name:: New New Directory New Name:  No Not exist directory  Open Last Connection Open Selected File. Modifications will not be saved   Pasive Password Port Queu Remember Remote Rename Rename Directory Rename File Rename Server Directory Show Show Remote folders tree Show Server Conversation Size Store Connection Subir Archivos Seleccionados This is Free Software. Enjoy It. Upload Canceled User Ver el archivo seleccionado View View Server Conversation Wrong Connection Data or Server unavailable Yes Project-Id-Version: gbftp 3.16.3
PO-Revision-Date: 2022-02-24 11:23 UTC
Last-Translator: jorge <jorge@abu>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 
No se pudo Crear el Directorio 
En:  Existe en el Servidor 21 : Renombrar Fichero Remoto ?. Solo se pueden eliminar directorios vacíos. Acerca de Activo Después de conectar: No se puede abrir el archivo descargado o el archivo no existe No puedo recordar una conexión sin nombre Carpeta Remota: Cerrar Conexión Cerrar la Conexión Actual Conectar Conexión Gestor de Conexiones Crear Nuevo Directorio Borrar ¿Eliminar directorio  Eliminar directorio vacío Borrar archivo ¿Eliminar archivos remotos seleccionados? Esto no se puede deshacer No se pudo eliminar. Compruebe si el directorio está vacío y los permisos son suficientes. Descargar Archivos Seleccionados Descargar Descarga cancelada Descargar Directorio y todo su contenido Descargar Seleccionado Error:
 Error: Error: No se pudo renombrar el archivo remoto Salida Cliente FTP, Hecho con Gambas 3 Nombre del Fichero Cliente FTP hecho en Gambas. Como Filezilla, pero más simple GBFTP Gambas FTP Gambas ftp: Gestor de Conexiones Host Conexiones conocidas Última modificación Local Hecho en 2021-2022 por Jorge Carrión (shordi@gmail.com) sin Copyrights Modo Nombre Nombre: Nuevo Nuevo directorio Nuevo Nombre: No No existe directorio Abrir última conexión Abrir archivo seleccionado. Las modificaciones no se guardarán Pasivo Contraseña Puerto Cola Recordar Remoto Renombrar Renombrar directorio Renombrar archivo Renombrar el directorio del servidor Show Mostrar árbol de carpetas remotas Mostrar conversación del servidor Tamaño Guardar la Conexión Subir Archivos Seleccionados Esto es Software Libre. Disfrútala. Carga cancelada Usuario Ver el archivo seleccionado Vista Ver Conversación con el Servidor Datos de conexión incorrectos o Servidor no disponible Sí 