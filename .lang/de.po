#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: gbftp 3.16.3\n"
"PO-Revision-Date: 2022-02-24 11:24 UTC\n"
"Last-Translator: jorge <jorge@abu>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: .project:1
msgid "GBFTP"
msgstr "GBFTP"

#: .project:2
msgid "Ftp Client made In Gambas. Like Filezilla, but simpler"
msgstr "FTP-Client in Gambas erstellt. Wie Filezilla, aber einfacher"

#: FMain.form:32
msgid "Connection"
msgstr "Verbindung"

#: FMain.form:35
msgid "Open Last Connection"
msgstr "Letzte Verbindung öffnen"

#: FMain.form:39
msgid "Known Connections"
msgstr "Bekannte Verbindungen"

#: FMain.form:43
msgid "Close Connection"
msgstr "Verbindung schließen"

#: FMain.form:48
msgid "View"
msgstr "Sicht"

#: FMain.form:51
msgid "View Server Conversation"
msgstr "Servergespräch anzeigen"

#: FMain.form:56
msgid "Exit"
msgstr "Ausgang"

#: FMain.form:59
msgid "About"
msgstr "Über"

#: FMain.form:74
msgid "Connections Manager"
msgstr "Verbindungsmanager"

#: FMain.form:86
msgid "Close Current Connection"
msgstr "Aktuelle Verbindung schließen"

#: FMain.form:92
msgid "Show Server Conversation"
msgstr "Servergespräch anzeigen"

#: FMain.form:100
msgid "Show Remote folders tree"
msgstr "Remote-Ordnerbaum anzeigen"

#: Fftp.class:41 fConnManager.form:146
msgid "Name"
msgstr "Name"

#: Fftp.class:43
msgid "Size"
msgstr "Größe"

#: Fftp.class:46
msgid "Last Modified"
msgstr "Zuletzt geändert"

#: Fftp.class:313
msgid "Create New Directory"
msgstr "Neues Verzeichnis erstellen"

#: Fftp.class:313
msgid "Name::"
msgstr "Name::"

#: Fftp.class:423
msgid "Download Canceled"
msgstr "Herunterladen abgebrochen"

#: Fftp.class:613
msgid "\nCan't Create Directory "
msgstr "\nVerzeichnis kann nicht erstellt werden"

#: Fftp.class:613
msgid "Error:\n"
msgstr "Fehler:\n"

#: Fftp.class:643
msgid "Upload Canceled"
msgstr "Hochladen abgebrochen"

#: Fftp.class:648
msgid "Error: "
msgstr "Fehler:"

#: Fftp.class:707
msgid "Delete selected remote files? This can't be undone"
msgstr "Ausgewählte Remote-Dateien löschen? Dies kann nicht rückgängig gemacht werden"

#: Fftp.class:707
msgid "No"
msgstr "Nein"

#: Fftp.class:707
msgid "Yes"
msgstr "Jawohl"

#: Fftp.class:724
msgid "\nIn: "
msgstr "\nIm:"

#: Fftp.class:746
msgid "Can't open downloaded file or file doesn't exist"
msgstr "Heruntergeladene Datei kann nicht geöffnet werden oder Datei existiert nicht"

#: Fftp.class:960
msgid ": Rename Remote File"
msgstr ": Remote-Datei umbenennen"

#: Fftp.class:960
msgid "New Name: "
msgstr "Neuer Name:"

#: Fftp.class:965
msgid " Exist on Server"
msgstr " Auf dem Server vorhanden"

#: Fftp.class:965
msgid "File Name "
msgstr "Dateiname"

#: Fftp.class:976
msgid "Error: Can't rename Remote File"
msgstr "Fehler: Remote-Datei kann nicht umbenannt werden"

#: Fftp.class:1016
msgid "Rename Server Directory"
msgstr "Serververzeichnis umbenennen"

#: Fftp.class:1048
msgid "Not exist directory "
msgstr "Verzeichnis nicht vorhanden"

#: Fftp.class:1052
msgid "?. Onle empty directories can be deleted."
msgstr "?. Es können nur leere Verzeichnisse gelöscht werden."

#: Fftp.class:1052
msgid "Delete Directory "
msgstr "Verzeichnis löschen"

#: Fftp.class:1062
msgid "Deletion failed. Check if directory is empty and permissions are enough."
msgstr "Löschen fehlgeschlagen. Überprüfen Sie, ob das Verzeichnis leer ist und die Berechtigungen ausreichen."

#: Fftp.form:65
msgid "Download Selected"
msgstr "Ausgewählt herunterladen"

#: Fftp.form:70
msgid "New Directory"
msgstr "Neues Verzeichnis"

#: Fftp.form:75
msgid "Show"
msgstr "Show"

#: Fftp.form:80
msgid "Rename"
msgstr "Umbenennen"

#: Fftp.form:85 fConnManager.form:56
msgid "Delete"
msgstr "Löschen"

#: Fftp.form:93
msgid "Download"
msgstr "Herunterladen"

#: Fftp.form:134
msgid "Local"
msgstr "Lokal"

#: Fftp.form:152
msgid "Subir Archivos Seleccionados"
msgstr "Subir Archivos Seleccionados"

#: Fftp.form:158
msgid "Ver el archivo seleccionado"
msgstr "Ver el archivo seleccionado"

#: Fftp.form:179
msgid "Remote"
msgstr "Fernbedienung"

#: Fftp.form:196
msgid "Carpeta Remota:"
msgstr "Carpeta Remota:"

#: Fftp.form:276
msgid "Download Directory and All Content"
msgstr "Verzeichnis und alle Inhalte herunterladen"

#: Fftp.form:281
msgid "Delete Empty Directory"
msgstr "Leeres Verzeichnis löschen"

#: Fftp.form:286
msgid "Rename Directory"
msgstr "Verzeichnis umbenennen"

#: Fftp.form:296
msgid "Descargar Archivos Seleccionados"
msgstr "Descargar Archivos Seleccionados"

#: Fftp.form:302
msgid "Open Selected File. Modifications will not be saved  "
msgstr "Ausgewählte Datei öffnen. Änderungen werden nicht gespeichert"

#: Fftp.form:308
msgid "Rename File"
msgstr "Datei umbenennen"

#: Fftp.form:314
msgid "Delete File"
msgstr "Datei löschen"

#: Fftp.form:334
msgid "Queu"
msgstr "Warteschlange"

#: fAbout.form:22
msgid "Gambas FTP"
msgstr "Gambas FTP"

#: fAbout.form:30
msgid "FTP Client, Made with Gambas 3"
msgstr "FTP-Client, erstellt mit Gambas 3"

#: fAbout.form:38
msgid "Made in 2021-2022 by Jorge Carrión (shordi@gmail.com) without Copyrights"
msgstr "Hergestellt in 2021-2022 von Jorge Carrión (shordi@gmail.com) ohne Urheberrechte"

#: fAbout.form:44
msgid "This is Free Software. Enjoy It."
msgstr "Dies ist Freie Software. Geniesse es."

#: fConnManager.class:47
msgid "Wrong Connection Data or Server unavailable"
msgstr "Falsche Verbindungsdaten oder Server nicht verfügbar"

#: fConnManager.class:53
msgid "Can't remember a unnamed connection"
msgstr "Kann mich nicht an eine unbenannte Verbindung erinnern"

#: fConnManager.form:32
msgid "Gambas ftp: Connections Manager"
msgstr "Gambas ftp: Verbindungsmanager"

#: fConnManager.form:62
msgid "New"
msgstr "Neu"

#: fConnManager.form:86
msgid "21"
msgstr "21"

#: fConnManager.form:92
msgid "Active"
msgstr "Aktiv"

#: fConnManager.form:92
msgid "Pasive"
msgstr "Passiv"

#: fConnManager.form:120
msgid "After Connect:"
msgstr "Nach Verbinden:"

#: fConnManager.form:125
msgid "Store Connection"
msgstr "Verbindung speichern"

#: fConnManager.form:126
msgid "Remember"
msgstr "Erinnere dich"

#: fConnManager.form:133
msgid "Connect"
msgstr "Anschließen"

#: fConnManager.form:140
msgid "Host"
msgstr "Gastgeber"

#: fConnManager.form:152
msgid "User"
msgstr "Benutzer"

#: fConnManager.form:158
msgid "Password"
msgstr "Passwort"

#: fConnManager.form:164
msgid "Port"
msgstr "Hafen"

#: fConnManager.form:170
msgid "Modo"
msgstr "Mode"

