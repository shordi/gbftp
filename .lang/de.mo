��    O      �  k         �     �     �     �     �     �  )        +     1     8  0   G  #   x     �     �     �     �  
   �     �     �               +     B  2   N  H   �      �     �     �  "   	     )	     ;	     C	     K	     k	     p	  
   �	  6   �	     �	  
   �	     �	     
     
     
     '
  I   -
     w
     |
     �
     �
     �
  
   �
     �
     �
     �
  5   �
                         "     +     2     9     J     V     n     s     �     �     �     �      �     �     	          *     /  +   H     t  �   x  '   K     s     x     �     �  7   �     �     �     �  M     6   Q     �     �     �     �  
   �     �     �               2     N  Q   ]  i   �           :     H  *   b     �     �     �  0   �     �  !   �  	     <        Z  
   `     k  	   �     �     �     �  Q   �                    %     )     ;     G     L     h  @   �     �     �     �     �     �     �  
             %     6     S     X     s     �     �     �  %   �     �               '     -  5   F     |     2                (   0                   =          9   .   6       '   /      $      3   "   8      <   M                             7       5                  K   !   ,   N         #          O                     C      4   L   %   F   B       I      
   -      E           +                 H   G      ?   J   &   A          1       >   	   @      D      )      *             ;          :    
Can't Create Directory  
In:   Exist on Server 21 : Rename Remote File ?. Onle empty directories can be deleted. About Active After Connect: Can't open downloaded file or file doesn't exist Can't remember a unnamed connection Carpeta Remota: Close Connection Close Current Connection Connect Connection Connections Manager Create New Directory Delete Delete Directory  Delete Empty Directory Delete File Delete selected remote files? This can't be undone Deletion failed. Check if directory is empty and permissions are enough. Descargar Archivos Seleccionados Download Download Canceled Download Directory and All Content Download Selected Error:
 Error:  Error: Can't rename Remote File Exit FTP Client, Made with Gambas 3 File Name  Ftp Client made In Gambas. Like Filezilla, but simpler GBFTP Gambas FTP Gambas ftp: Connections Manager Host Known Connections Last Modified Local Made in 2021-2022 by Jorge Carrión (shordi@gmail.com) without Copyrights Modo Name Name:: New New Directory New Name:  No Not exist directory  Open Last Connection Open Selected File. Modifications will not be saved   Pasive Password Port Queu Remember Remote Rename Rename Directory Rename File Rename Server Directory Show Show Remote folders tree Show Server Conversation Size Store Connection Subir Archivos Seleccionados This is Free Software. Enjoy It. Upload Canceled User Ver el archivo seleccionado View View Server Conversation Wrong Connection Data or Server unavailable Yes Project-Id-Version: gbftp 3.16.3
PO-Revision-Date: 2022-02-24 11:24 UTC
Last-Translator: jorge <jorge@abu>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 
Verzeichnis kann nicht erstellt werden 
Im:  Auf dem Server vorhanden 21 : Remote-Datei umbenennen ?. Es können nur leere Verzeichnisse gelöscht werden. Über Aktiv Nach Verbinden: Heruntergeladene Datei kann nicht geöffnet werden oder Datei existiert nicht Kann mich nicht an eine unbenannte Verbindung erinnern Carpeta Remota: Verbindung schließen Aktuelle Verbindung schließen Anschließen Verbindung Verbindungsmanager Neues Verzeichnis erstellen Löschen Verzeichnis löschen Leeres Verzeichnis löschen Datei löschen Ausgewählte Remote-Dateien löschen? Dies kann nicht rückgängig gemacht werden Löschen fehlgeschlagen. Überprüfen Sie, ob das Verzeichnis leer ist und die Berechtigungen ausreichen. Descargar Archivos Seleccionados Herunterladen Herunterladen abgebrochen Verzeichnis und alle Inhalte herunterladen Ausgewählt herunterladen Fehler:
 Fehler: Fehler: Remote-Datei kann nicht umbenannt werden Ausgang FTP-Client, erstellt mit Gambas 3 Dateiname FTP-Client in Gambas erstellt. Wie Filezilla, aber einfacher GBFTP Gambas FTP Gambas ftp: Verbindungsmanager Gastgeber Bekannte Verbindungen Zuletzt geändert Lokal Hergestellt in 2021-2022 von Jorge Carrión (shordi@gmail.com) ohne Urheberrechte Mode Name Name:: Neu Neues Verzeichnis Neuer Name: Nein Verzeichnis nicht vorhanden Letzte Verbindung öffnen Ausgewählte Datei öffnen. Änderungen werden nicht gespeichert Passiv Passwort Hafen Warteschlange Erinnere dich Fernbedienung Umbenennen Verzeichnis umbenennen Datei umbenennen Serververzeichnis umbenennen Show Remote-Ordnerbaum anzeigen Servergespräch anzeigen Größe Verbindung speichern Subir Archivos Seleccionados Dies ist Freie Software. Geniesse es. Hochladen abgebrochen Benutzer Ver el archivo seleccionado Sicht Servergespräch anzeigen Falsche Verbindungsdaten oder Server nicht verfügbar Jawohl 